function modal() {
  document.getElementById('social-items').style.cssText = 'height:auto;min-height:max-content;width:12rem;max-width:100%;opacity:1;';
}

function reset() {
  document.getElementById('social-items').style.cssText = 'height:0px;width:0px;opacity:1;';

}

function modal2() {
  document.getElementById('social-items').style.cssText = 'height:96px;opacity:1;';
}

function reset2() {
  document.getElementById('social-items').style.cssText = 'height:0px;opacity:0;';
}

function toggle() {
  var x = document.getElementById("social-items");
  if (x.style.height === "0px") {
    modal();
  } else {
    reset();
  }
}

function toggle2() {
  var x = document.getElementById("social-items");
  if (x.style.height === "0px") {
    modal2();
  } else {
    reset2();
  }
}
